package com.example.sean.yourfault;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.graphics.Camera;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.wearable.Wearable;

import java.util.HashMap;

public class IndividualEarthquakeActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleApiClient mGoogleApiClient;
    public static final String TAG = IndividualEarthquakeActivity.class.getSimpleName();
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private final static int UPDATE_INTERVAL_MS = 10 * 1000;
    private final static int FASTEST_INTERVAL_MS = 1000;
    private LocationRequest mLocationRequest;
    boolean mShowMap;
    GoogleMap mMap;
    public Intent data;
    private String specificPlace;
    private String specificMagnitude;
    private double earthquakeLong;
    private double earthquakeLat;
    private TextView place;
    private TextView distance;
    private TextView magnitude;
    private Button imageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual_earthquake);
        data = getIntent();
        mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API).addApi(Wearable.API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();

        mShowMap = initMap();

        place = (TextView) findViewById(R.id.individualPlace);
        distance = (TextView) findViewById(R.id.individualDistance);
        magnitude = (TextView) findViewById(R.id.individualMagnitude);
        imageButton = (Button) findViewById(R.id.goToImages);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
        specificPlace = data.getStringExtra("Place");
        specificMagnitude = data.getStringExtra("Magnitude");
        earthquakeLong = data.getDoubleExtra("Longitude", 0);
        earthquakeLat = data.getDoubleExtra("Latitude", 0);
        if (mShowMap) {
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(earthquakeLat, earthquakeLong), 5);
            mMap.moveCamera(update);
        }
        final Intent pictures = new Intent(this, pictureActivity.class);
        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                pictures.putExtra("picLong", earthquakeLong);
                pictures.putExtra("picLat", earthquakeLat);
                startActivity(pictures);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL_MS)
                .setFastestInterval(FASTEST_INTERVAL_MS);
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            // Blank for a moment...
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
        else {
            handleNewLocation(location);
        };
    }

    @Override
    public void onConnectionSuspended(int i){

    }

    @Override
    public void onConnectionFailed(ConnectionResult connResult) {
        if (connResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void handleNewLocation(Location location) {
        double yourLong = location.getLongitude();
        double yourLat = location.getLatitude();
        long dist = find_distance(yourLong, yourLat, earthquakeLong, earthquakeLat);
        String distString = String.valueOf(dist) + " km";
        place.setText(specificPlace);
        magnitude.setText(specificMagnitude);
        distance.setText(distString);

    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    private boolean initMap() {
        if (mMap == null) {
            MapFragment mapFrag = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
            mMap = mapFrag.getMap();
        }
        return (mMap != null);
    }

    public long find_distance(double yourLong, double yourLat, double eLong, double eLat) {
        int R = 637100;
        double circle1 = Math.toRadians(yourLat);
        double circle2 = Math.toRadians(eLat);
        double triangle1 = Math.toRadians(eLat - yourLat);
        double triangle2 = Math.toRadians(eLong - yourLong);
        double a = Math.sin(triangle1/2) * Math.sin(triangle1/2) + Math.cos(circle1) * Math.cos(circle2) * Math.sin(triangle2/2) * Math.sin(triangle2/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c;
        double kiloD = d/1000;
        return Math.round(kiloD);
    }

}
