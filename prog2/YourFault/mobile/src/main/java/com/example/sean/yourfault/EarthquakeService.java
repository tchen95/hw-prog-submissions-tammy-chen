package com.example.sean.yourfault;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class EarthquakeService extends Service {

    public final String ACTION_GET_EARTHQUAKE_DATA = "EARTHQUAKE_INTENT";
    HttpURLConnection connection = null;
    Intent earthquakeData;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        earthquakeData = new Intent();
        earthquakeData.setAction(ACTION_GET_EARTHQUAKE_DATA);
        Runnable queryRun = new Runnable() {
            @Override
            public void run() {
                try{
                    String stringUrl = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_day.geojson";
                    URL earthquakeURL = new URL(stringUrl);
                    connection = (HttpURLConnection)earthquakeURL.openConnection();
                    InputStream in = new BufferedInputStream(connection.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);

                    }
                    String stream = sb.toString();
                    JSONObject jsonData = new JSONObject(stream);
                    JSONArray features = jsonData.getJSONArray("features");
                    String[] entries = new String[features.length()];
                    String[] places = new String[features.length()];
                    double[] longArray = new double[features.length()];
                    double[] latArray = new double[features.length()];
                    HashMap<String, String> placeMag = new HashMap<>();
                    for (int i = 0; i < features.length(); i++) {
                        JSONObject element = features.getJSONObject(i);
                        JSONObject properties = element.getJSONObject("properties");
                        JSONObject geometry = element.getJSONObject("geometry");
                        String place = properties.getString("place");
                        String finalPlace = parsePlace(place);
                        String magnitude = properties.getString("mag");
                        JSONArray coordinates = geometry.getJSONArray("coordinates");
                        double longitude = coordinates.getDouble(0);
                        double latitude = coordinates.getDouble(1);
                        String concat = magnitude + "M  " + finalPlace;
                        entries[i] = concat;
                        places[i] = finalPlace;
                        longArray[i] = longitude;
                        latArray[i] = latitude;
                        placeMag.put(finalPlace, magnitude + " M");
                    }
                    earthquakeData.putExtra("Places", places);
                    earthquakeData.putExtra("Main Entries", entries);
                    earthquakeData.putExtra("placeMag", placeMag);
                    earthquakeData.putExtra("Longitudes", longArray);
                    earthquakeData.putExtra("Latitudes", latArray);
                    sendBroadcast(earthquakeData);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    stopSelf();
                    connection.disconnect();
                }
            }
        };
        Thread earthquakeThread = new Thread(queryRun);
        earthquakeThread.start();
        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public String parsePlace(String place) {
        int count = place.indexOf("f");
        count += 2;
        String finalPlace = "";
        while (count < place.length()) {
            finalPlace += place.charAt(count);
            count++;
        }
        return finalPlace;
    }
}
