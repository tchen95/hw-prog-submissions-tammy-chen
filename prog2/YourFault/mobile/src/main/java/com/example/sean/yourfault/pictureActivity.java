package com.example.sean.yourfault;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.InputStream;

public class pictureActivity extends Activity {

    private BroadcastReceiver myReceiver;
    public final String RETRIEVE_PICTURES = "GET_PICTURES";
    public Intent data;
    private double picLat;
    private double picLong;
    private String imageUrls[];
    private int currImage = 0;
    private int imageCap;

    private class ImageDownloader extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public ImageDownloader(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String url = urls[0];
            Bitmap bitmap = null;
            try {
                InputStream in = new java.net.URL(url).openStream();
                bitmap = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.d("pictureActivity", e.getMessage());
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture);
        setImageRotateListener();
        data = getIntent();
        picLat = data.getDoubleExtra("picLat", 0);
        picLong = data.getDoubleExtra("picLong", 0);
    }

    @Override
    protected void onStart() {
        Intent getPictures = new Intent(this, pictureService.class);
        getPictures.putExtra("picLat", picLat);
        getPictures.putExtra("picLong", picLong);
        startService(getPictures);
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        myReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent data) {
                imageUrls = data.getStringArrayExtra("photoURLs");
                imageCap = data.getIntExtra("count", 0);
                setCurrentImage();
            }
        };
        IntentFilter pictureFilter = new IntentFilter(RETRIEVE_PICTURES);
        registerReceiver(myReceiver, pictureFilter);

    }

    private void setImageRotateListener() {
        final Button rotatebutton = (Button) findViewById(R.id.btnRotateImage);
        rotatebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currImage++;
                if (currImage == imageCap) {
                    currImage = 0;
                }
                setCurrentImage();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(myReceiver);
    }

    private void setCurrentImage() {
        final ImageView imageView = (ImageView) findViewById(R.id.imageDisplay);
        ImageDownloader imageDownloader = new ImageDownloader(imageView);
        imageDownloader.execute(imageUrls[currImage]);
    }
}
