package com.example.sean.yourfault;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Sean on 10/19/15.
 */
public class pictureService extends Service {

    public final String RETRIEVE_PICTURES = "GET_PICTURES";
    HttpURLConnection connection = null;
    Intent pictureData;
    private double picLat;
    private double picLong;
    private String stringLat;
    private String stringLon;
    private String latRoundDown;
    private String lonRoundUp;
    private String bbox;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        pictureData = new Intent();
        pictureData.setAction(RETRIEVE_PICTURES);
        picLat = intent.getDoubleExtra("picLat", 0);
        picLong = intent.getDoubleExtra("picLong", 0);
        Runnable queryRun = new Runnable() {
            @Override
            public void run() {
                try {
                    stringLat = String.valueOf(picLat);
                    stringLon = String.valueOf(picLong);
                    String stringUrl = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=8b53f8b37e18b4c025cfc32fe0fb7c50&lat=" + stringLat + "&lon=" + stringLon + "&radius=20" + "&format=json&nojsoncallback=1"; ;

                    URL earthquakeURL = new URL(stringUrl);
                    connection = (HttpURLConnection) earthquakeURL.openConnection();
                    InputStream in = new BufferedInputStream(connection.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                    String stream = sb.toString();
                    JSONObject jsonData = new JSONObject(stream);
                    JSONObject photos = jsonData.getJSONObject("photos");
                    int page = photos.getInt("page");
                    JSONArray photo= photos.getJSONArray("photo");
                    int photoCount = 10;
                    if (photo.length() < 10) {
                        photoCount = photo.length();
                    }
                    String[] photoURLs = new String[photoCount];
                    for (int i = 0; i < photoCount; i++) {
                        JSONObject singularPhoto = photo.getJSONObject(i);
                        String farmID = singularPhoto.getString("farm");
                        String serverID= singularPhoto.getString("server");
                        String photoID = singularPhoto.getString("id");
                        String secretID = singularPhoto.getString("secret");
                        String singularURL = "https://farm" + farmID + ".staticflickr.com/" + serverID + "/" + photoID + "_" + secretID + "_b.jpg";
                        photoURLs[i] = singularURL;
                    }
                    pictureData.putExtra("photoURLs", photoURLs);
                    pictureData.putExtra("count", photoCount);
                    sendBroadcast(pictureData);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    stopSelf();
                    connection.disconnect();
                }
            }
        };
        Thread earthquakeThread = new Thread(queryRun);
        earthquakeThread.start();
        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
