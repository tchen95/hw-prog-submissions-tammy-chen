package com.example.sean.yourfault;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.HashMap;


public class MainActivity extends AppCompatActivity {
    private ListView listview;
    private ArrayAdapter<String> adapter;
    public final String ACTION_GET_EARTHQUAKE_DATA = "EARTHQUAKE_INTENT";
    public final String ACTION_INDIVIDUAL_PLACE_MAG = "EARTHQUAKE_PLACE_MAG";
    private BroadcastReceiver myReceiver;
    private String[] values;
    private String[] places;
    private double[] longs;
    private double[] lats;
    private HashMap<String, String> placeMag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listview = (ListView) findViewById(R.id.list);
    }

    @Override
    protected void onStart() {
        Intent send = new Intent(this, EarthquakeService.class);
        startService(send);
        super.onStart();


    }

    @Override
    protected  void onResume() {
        super.onResume();
        myReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent data) {
                values = data.getStringArrayExtra("Main Entries");
                places = data.getStringArrayExtra("Places");
                placeMag = (HashMap<String, String>) data.getSerializableExtra("placeMag");
                longs = data.getDoubleArrayExtra("Longitudes");
                lats = data.getDoubleArrayExtra("Latitudes");
                adapter = new ArrayAdapter<>(context, android.R.layout.activity_list_item, android.R.id.text1, values);
                listview.setAdapter(adapter);
                final Intent individualEarthquake = new Intent(context, IndividualEarthquakeActivity.class);
                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String specificPlace = places[position];
                        String specificMagnitude = placeMag.get(specificPlace);
                        double specificLong = longs[position];
                        double specificLat = lats[position];
                        individualEarthquake.putExtra("Place", specificPlace);
                        individualEarthquake.putExtra("Magnitude", specificMagnitude);
                        individualEarthquake.putExtra("Longitude", specificLong);
                        individualEarthquake.putExtra("Latitude", specificLat);
                        startActivity(individualEarthquake);
                    }
                });
            }
        };
        IntentFilter earthquakeIntentFilter = new IntentFilter(ACTION_GET_EARTHQUAKE_DATA);
        registerReceiver(myReceiver, earthquakeIntentFilter);
    }

    @Override protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(myReceiver);
    }
}
